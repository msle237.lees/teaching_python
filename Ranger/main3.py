# How to import libraries and use them for ROV

# First were gonna install pygame. pip install pygame
# pygame in our use cases is going to be used to take data from the joysticks and convert them to useable data for the ROV.

# so the way you import libraries in python is pretty simple.
import pygame

# thats it, pygame is now imported into the program. to test if you have pygame installed correctly run the program in the command prompt

# next up we need to tell the python script to initialize the pygame library for later use
